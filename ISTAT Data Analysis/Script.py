import numpy as np
import matplotlib
from matplotlib import pyplot as plt



def  fitData(Nimprese,ForzaLavPerTassoDisocc,incertezze):
    #Per una maggiore fittabilità moltiplico per 10 Nimprese e ForzaLav/TassoDisocc in modo da evitare numeri troppo piccoli che generino divergenze. NON FUNZIA

    dataFitted = np.polyfit(Nimprese,ForzaLavPerTassoDisocc,1,full=True,w=incertezze)
    m = dataFitted[0][0]
    x=np.linspace(0,120000,1000)
    q = dataFitted[0][1]
    q = dataFitted[0][1]

    y= m*x+q
    yAttesi = m*Nimprese+q
    
    #calcolo indice di correlazione
    indiceDiCorrelazione = calculateCorrelationIndex(Nimprese,ForzaLavPerTassoDisocc);
    print("INDICE DI CORRELAZIONE: "+ str(indiceDiCorrelazione));

    #calcolo chi2
    chi2 = calculateChi2(yAttesi,ForzaLavPerTassoDisocc,incertezze)
    print("CHI2: "+str(chi2))
    FitResult = [x,y,chi2]
    return FitResult


def calculateChi2(yteor,ysper,dy):
    ydiff=yteor-ysper
    residualWeighted = (ydiff)/dy;
    chi2 = np.sum((residualWeighted)**2);
    return chi2;
    

def calculateCorrelationIndex(dati1,dati2):
    mediaDati1 = np.mean(dati1);
    mediaDati2 = np.mean(dati2);
    varianza1 = np.sum((dati1-mediaDati1)**2);
    varianza2 = np.sum((dati2-mediaDati2)**2);
    covarianza12 = np.sum((dati1-mediaDati1)*(dati2-mediaDati2));
    print("varianza2" + np.str(varianza2));
    print("PRODOTTO VARIANZE" + np.str(varianza1*varianza2));
    print("COVARIANZA" + np.str(covarianza12));
    correlationIndex = covarianza12/np.sqrt(varianza1*varianza2);
    return correlationIndex;
    


def plotData(Nimprese,ForzaLavPerTassoDisocc,FitResult,incertezze):
    x = FitResult[0]
    y = FitResult[1]
    plt.errorbar(Nimprese,ForzaLavPerTassoDisocc,xerr=0,yerr=incertezze,fmt='.k')
    plt.plot(x,y,'r-')
    plt.xlabel("Number of companies")
    plt.ylabel("Workforce/(Unemployment Rate[%])")
    plt.savefig("Numero aziende vs tasso disoccupazione.png")
    plt.show()
    return





def matchAziendeVSDisocc(numeroImprese, tassoDisoccupazione, forzaDiLavoroProvince):
    NAziendeVSDisocc = np.zeros((numeroImprese.shape[0],2))
    forzaLavoro=0.0

    for i in range (0,numeroImprese.shape[0]):
        for f in range (0, forzaDiLavoroProvince.shape[0]):
            if forzaDiLavoroProvince[f][0] == numeroImprese[i][0]:
                forzaLavoro = np.float(forzaDiLavoroProvince[f][1].replace(".","").replace(",","."))
                break
              
        if forzaLavoro==0:
            print("Forza lavoro non trovata per " + numeroImprese[i][0])
              
        NAziendeVSDisocc[i][0] = np.float(numeroImprese[i][1].replace(".","").replace(",","."))
    
        for j in range (0,tassoDisoccupazione.shape[0]):
            if numeroImprese[i][0] == tassoDisoccupazione[j][0]:
                NAziendeVSDisocc[i][1] = forzaLavoro/np.float(tassoDisoccupazione[j][1].replace(",","."))
                break

        if NAziendeVSDisocc[i][1]==0:
            print("Tasso disoccupazione non trovato per " + numeroImprese[i][0])
            
    return NAziendeVSDisocc



def valutazioneErrori(a,w,u,da,dw,du):
       
    dy = 1.0/(u**2)*(u*dw+w*du)+0.0005*a
    return dy

    



numeroImprese = np.loadtxt("NumImprese.txt", dtype=np.str)
tassoDisoccupazione = np.loadtxt("TassoDisocc.txt", dtype=np.str)
forzaDiLavoroProvince=np.loadtxt("Forza di lavoro.txt", dtype=np.str)
print("Numero imprese: " + np.str(numeroImprese.shape))
print("Tasso di disoccupazione: "+np.str(tassoDisoccupazione.shape))
#print("EstensioneProvince: "+np.str(estensioniProvince.shape))
print("Forza di lavoro.txt: "+np.str(forzaDiLavoroProvince.shape))
#print(numeroImprese)
#print(estensioniProvince)
#print(forzaDiLavoroProvince)
#print(tassoDisoccupazione)
NAziendeVSDisocc = matchAziendeVSDisocc(numeroImprese, tassoDisoccupazione, forzaDiLavoroProvince)                                               
NImprese = NAziendeVSDisocc[:,0]
TassoDisocc = NAziendeVSDisocc[:,1]
ForzaLavPerTassoDisocc = NAziendeVSDisocc[:,1]
ForzaLav = ForzaLavPerTassoDisocc*TassoDisocc

incertezze = valutazioneErrori(NImprese,ForzaLav, TassoDisocc,10,10,5)
print(incertezze)
FitResult= fitData(NImprese, ForzaLavPerTassoDisocc,incertezze)
plotData(NImprese,ForzaLavPerTassoDisocc,FitResult,incertezze)


#Testiamo indice di correlazione
#A = np.array([1,1,4]);
#B = np.array([1.3,0.5,3]);

#c = calculateCorrelationIndex(A,B);
#chi2 = calculateChi2(A,B,1);
#print("c = " + np.str(c) + " "+ "chi2 = "+np.str(chi2));
    
