import os
import matplotlib
from matplotlib import pyplot as plt
import numpy
import sympy
from sympy import symbols
from sympy.solvers import solve
from sympy import *
from sympy import init_printing
import scipy
from scipy import integrate
import pylab
import mpl_toolkits.mplot3d.axes3d as p3
from sympy import sqrt


import Anima
from Anima import Anima

t = symbols('t')
x,y,z,r,theta,phi,xdot,ydot,zdot,rdot,thetadot,phidot = \
                                                      symbols('x y z r theta phi xdot ydot zdot rdot thetadot phidot', cls=Function)
x=x(t)
y=y(t)
z=z(t)
r=r(t)
theta=theta(t)
phi=phi(t)
xdot=xdot(t)
ydot=ydot(t)
zdot=zdot(t)
rdot=rdot(t)
thetadot=thetadot(t)
phidot=phidot(t)
os.system('clear')



#Definisco qualche simbolo di comodo
T=xdot**2+ydot**2+zdot**2
G=1/(sqrt(x**2+y**2+z**2))



print('\n')
print('*************************************************************************\n')
print('                    PRINCIPIO  DI  MINIMA AZIONE          \n')
print('*************************************************************************\n')
print('\n\n')
print('Per le coordinate cartesiane usare x,y,z. \n\
Per le coordinate polari usare r, theta, phi.\n\
Per ottenere le derivate temporali \n\
aggiungere  "dot" a una coordinata,\n\
per esempio xdot=dx/dt.\n\n\
Inserire la Lagrangiana:\n\n')
L=input('L=')
init_printing()
pprint(L)
L=N(L)
#u = vero se si usano coord. cartesiane, falso se si usano coordinate sferiche

elle = str(L)
u = 'x' in elle or 'y' in elle or 'z' in elle

print('\n\n')
#Calcolo delle equazioni di Eulero Lagrange
XI = sympy.diff(sympy.diff(L,xdot),t)
XII = sympy.diff(L,x)
dXDOT =sympy.solve(XI-XII,Derivative(xdot,t),rational=False)
YI = sympy.diff(sympy.diff(L,ydot),t)
YII = sympy.diff(L,y)
dYDOT =sympy.solve(YI-YII,Derivative(ydot,t),rational=False)
ZI = sympy.diff(sympy.diff(L,zdot),t)
ZII = sympy.diff(L,z)
dZDOT =sympy.solve(ZI-ZII,Derivative(zdot,t),rational=False)
RI = sympy.diff(sympy.diff(L,rdot),t)
RII = sympy.diff(L,y)
dRDOT =sympy.solve(RI-RII,Derivative(ydot,t),rational=False)
THETAI = sympy.diff(sympy.diff(L,thetadot),t)
THETAII = sympy.diff(L,theta)
dTHETADOT =sympy.solve(THETAI-THETAII,Derivative(thetadot,t),rational=False)
PHII = sympy.diff(sympy.diff(L,thetadot),t)
PHIII = sympy.diff(L,theta)
dPHIDOT =sympy.solve(PHII-PHIII,Derivative(phidot,t),rational=False)

#SCRITTURA DEL SISTEMA DIFFERENZIALE DA RISOLVERE COME FUNZIONE
#Creo le stringhe, tolgo i '(t)' e le parentesi quadre iniziali
#(la funzione solve infatti restituisce '[espressione matematica]' 
#in modo da lasciare solo i simboli

if u:
        dXDOTSTR = str(dXDOT)[1:-1]
        if dXDOTSTR=='[]':
        	dXDOTSTR='[0]'
        
        dXDOTSTR = dXDOTSTR.replace('Derivative(x(t), t)', 'xdot')
        dXDOTSTR = dXDOTSTR.replace('Derivative(y(t), t)', 'ydot')
        dXDOTSTR = dXDOTSTR.replace('Derivative(z(t), t)', 'zdot')
        dXDOTSTR=dXDOTSTR.replace('xdot','X[3]')
        dXDOTSTR=dXDOTSTR.replace('ydot','X[4]')
        dXDOTSTR=dXDOTSTR.replace('zdot','X[5]')
        dXDOTSTR = dXDOTSTR.replace('(t)','')
        dXDOTSTR = dXDOTSTR.replace('x','X[0]')
        dXDOTSTR = dXDOTSTR.replace('y','X[1]')
        dXDOTSTR = dXDOTSTR.replace('z','X[2]')


        dYDOTSTR = str(dYDOT)[1:-1]
        if dYDOTSTR=='[]':
                dYDOTSTR='[0]'
    
        dYDOTSTR=dYDOTSTR.replace('Derivative(x(t), t)','X[3]')
        dYDOTSTR=dYDOTSTR.replace('Derivative(y(t), t)','X[4]')
        dYDOTSTR=dYDOTSTR.replace('Derivative(z(t), t)','X[5]')
        dYDOTSTR=dYDOTSTR.replace('xdot','X[3]')
        dYDOTSTR=dYDOTSTR.replace('ydot','X[4]')
        dYDOTSTR=dYDOTSTR.replace('zdot','X[5]')
        dYDOTSTR = dYDOTSTR.replace('(t)','')
        dYDOTSTR = dYDOTSTR.replace('x','X[0]')
        dYDOTSTR = dYDOTSTR.replace('y','X[1]')
        dYDOTSTR = dYDOTSTR.replace('z','X[2]')
        
        dZDOTSTR = str(dZDOT)[1:-1]
        if dZDOTSTR=='[]':
                dZDOTSTR='[0]'
   
        dZDOTSTR=dZDOTSTR.replace('Derivative(x(t), t)','X[3]')
        dZDOTSTR=dZDOTSTR.replace('Derivative(y(t), t)','X[4]')
        dZDOTSTR=dZDOTSTR.replace('Derivative(z(t), t)','X[5]')
        dZDOTSTR=dZDOTSTR.replace('xdot','X[3]')
        dZDOTSTR=dZDOTSTR.replace('ydot','X[4]')
        dZDOTSTR=dZDOTSTR.replace('zdot','X[5]')
        dZDOTSTR = dZDOTSTR.replace('(t)','') 
        dZDOTSTR = dZDOTSTR.replace('x','X[0]')	
        dZDOTSTR = dZDOTSTR.replace('y','X[1]')
        dZDOTSTR = dZDOTSTR.replace('z','X[2]')
      

else:
        dRDOTSTR = str(dRDOT)[1:-1]
        if dRDOTSTR=='[]':
                dRDOTSTR='[0]'
       
        dRDOTSTR=dRDOTSTR.replace('Derivative(r(t), t)','R[3]')
        dRDOTSTR=dRDOTSTR.replace('Derivative(theta(t), t)','R[4]')
        dRDOTSTR=dRDOTSTR.replace('Derivative(phi(t), t)','R[5]')
        dRDOTSTR=dRDOTSTR.replace('rdot','R[3]')
        dRDOTSTR=dRDOTSTR.replace('thetadot','R[4]')
        dRDOTSTR=dRDOTSTR.replace('phidot','R[5]')
        dRDOTSTR = dRDOTSTR.replace('(t)','') 
        dRDOTSTR = dRDOTSTR.replace('r','R[0]')
        dRDOTSTR = dRDOTSTR.replace('theta','R[1]')
        dRDOTSTR = dRDOTSTR.replace('phi','R[2]')
        dTHETADOTSTR = str(dTHETADOT)[1:-1]
        if dTHETADOTSTR=='[]':
                dTHETADOTSTR='[0]'
        N=NumeriDaStringa(dTHETADOTSTR)
        for i in range (0,len(N)):
                dTHETADOTSTR=dTHETADOTSTR.replace(str(N[i]),str(float(N[i])))
        dTHETADOTSTR=dTHETADOTSTR.replace('Derivative(r(t), t)','R[3]')
        dTHETADOTSTR=dTHETADOTSTR.replace('Derivative(theta(t), t)','R[4]')
        dTHETADOTSTR=dTHETADOTSTR.replace('Derivative(phi(t), t)','R[5]')
        dTHETADOTSTR=dTHETADOTSTR.replace('rdot','R[3]')
        dTHETADOTSTR=dTHETADOTSTR.replace('thetadot','R[4]')
        dTHETADOTSTR=dTHETADOTSTR.replace('phidot','R[5]')
        dTHETADOTSTR = dTHETADOTSTR.replace('(t)','') 
        dTHETADOTSTR = dTHETADOTSTR.replace('r','R[0]')
        dTHETADOTSTR = dTHETADOTSTR.replace('theta','R[1]')
        dTHETADOTSTR = dTHETADOTSTR.replace('phi','R[2]')
	
        dPHIDOTSTR = str(dPHIDOT)[1:-1]
        if dPHIDOTSTR=='[]':
                dPHIDOTSTR='[0]'
    
        dPHIDOTSTR=dPHIDOTSTR.replace('Derivative(r(t), t)','R[3]')
        dPHIDOTSTR=dPHIDOTSTR.replace('Derivative(theta(t), t)','R[4]')
        dPHIDOTSTR=dPHIDOTSTR.replace('Derivative(phi(t), t)','R[5]')
        dPHIDOTSTR=dPHIDOTSTR.replace('rdot','R[3]')
        dPHIDOTSTR=dPHIDOTSTR.replace('thetadot','R[4]')
        dPHIDOTSTR = dPHIDOTSTR.replace('phidot','R[5]')
        dPHIDOTSTR =dPHIDOTSTR.replace('(t)','') 
        dPHIDOTSTR = dPHIDOTSTR.replace('r','R[0]')
        dPHIDOTSTR =dPHIDOTSTR.replace('theta','R[1]')
        dPHIDOTSTR = dPHIDOTSTR.replace('phi','R[2]')

#Scrivo la funzione
if u:		
        SCart ="def FCartesiano(X,t):\n          import numpy\n          return [X[3],X[4],X[5],%s,%s,%s]"%(dXDOTSTR, dYDOTSTR, dZDOTSTR)
        SCart=SCart.replace('sqrt','numpy.sqrt')
        SCart=SCart.replace('sin','numpy.sin')
        SCart=SCart.replace('cos','numpy.cos')
        SCart=SCart.replace('tan','numpy.tan')
        SCart=SCart.replace('log','numpy.log')
        FUNZIONE_CARTESIANA=open('FCart.py','w')
        FUNZIONE_CARTESIANA.write(SCart)
        FUNZIONE_CARTESIANA.close()
else:
        SSfer ="def FSferico(R,t):\n          import numpy\n          return [R[3],R[4],R[5],%s,%s,%s]"%(dRDOTSTR, dTHETADOTSTR, dPHIDOTSTR)
        SSfer=SSfer.replace('sqrt','numpy.sqrt')
        SSfer=SSfer.replace('sin','numpy.sin')
        SSfer=SSfer.replace('cos','numpy.cos')
        SSfer=SSfer.replace('tan','numpy.tan')
        SSfer=SSfer.replace('log','numpy.log')
        FUNZIONE_SFERICA=open('FSf.py','w')
        FUNZIONE_SFERICA.write(SSfer)
        FUNZIONE_SFERICA.close()
	
	
#CHIEDIAMO LE CONDIZIONI INIZIALI
#cartesiane
print('Digita le condizioni iniziali:\n')
	
if u:
        x0=input('x0: ')
        xdot0=input('xdot0: ')
        y0=input('y0: ')
        ydot0=input('ydot0: ')
        z0=input('z0: ')
        zdot0=input('zdot0: ')


#coord. sferiche (metto come commento finche non riesco a selezionare)
else:
        r0=input('r0: ')
        rdot0=input('rdot0: ')
        theta0=input('theta0: ')
        thetadot0=input('thetadot0: ')
        phi0=input('phi0: ')
        phidot0=input('phidot0: ')

#Chiedo l'intervallo temporale in cui mostrare la particella
ti=input('Istante di tempo iniziale: ')
tf=input('Istante di tempo finale: ')


#FORMO GLI ARGOMENTI PER ODEINT
T = numpy.arange(ti,tf,0.01)
if u:
        X0 = [x0,y0,z0,xdot0,ydot0,zdot0]
else:
        R0 = [r0,theta0,phi0,rdot0,thetadot0,phidot0]

import FCart
import FSf
from FCart import FCartesiano
from FSf import FSferico
if u:
        SOLCartesio= integrate.odeint(FCartesiano,X0,T)
else:
        SOLSferico= integrate.odeint(FSferico,R0,T)



#Si muova la particella seguendo la Somma Traiettoria. E la Legge fu

if u:
        figC=pylab.figure()
        ax = p3.Axes3D(figC)
        ax.plot(SOLCartesio[:,0],SOLCartesio[:,1],SOLCartesio[:,2],'r--')
        pylab.show(figC)
	Anima(T, SOLCartesio[:,0],SOLCartesio[:,1],SOLCartesio[:,2])
else:
        X=SOLSferico[:,0]*sin(SOLSferico[:,1])*sin(SOLSferico[:,2])
        Y=SOLSferico[:,0]*sin(SOLSferico[:,1])*cos(SOLSferico[:,2])
        Z=SOLSferico[:,0]*cos(SOLSferico[:,1])
        figS=pylab.figure
        ar=p3.Axes3D(figS)
        ar.plot_wireframe(X,Y,Z,'*')
        pylab.show(figS)






