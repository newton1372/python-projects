import matplotlib
from matplotlib import pyplot as plt
import mpl_toolkits.mplot3d as p3
from matplotlib import animation
import numpy as np

def Anima(t,x,y,z):
	FIG=plt.figure()
	AX=p3.Axes3D(FIG)
	AX.set_xlim(min(x)*(1-0.05),max(x)*(1+0.05))
	AX.set_ylim(min(y)*(1-0.05),max(y)*(1+0.05))
	AX.set_zlim(min(z)*(1-0.05),max(z)*(1+0.05))
	LINE, = AX.plot([],[],[],marker='o',markersize=20)
	
	def Inizia():
        	LINE.set_data(x[0],y[0])
		LINE.set_3d_properties(z[0])
		return LINE,

        def Animazione(i):
                LINE.set_data(x[i],y[i])
		LINE.set_3d_properties(z[i])
                return LINE,

        ANIMAZIONE=animation.FuncAnimation(FIG,Animazione, np.arange(0,len(x)-1,1),init_func =Inizia, interval = 0.1, blit=True)
#	plt.show()
	FFwriter = animation.FFMpegWriter(fps=50)
	ANIMAZIONE.save('Video.mp4',writer=FFwriter)




